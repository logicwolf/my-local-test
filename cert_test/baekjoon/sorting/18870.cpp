#include <stdio.h>

typedef enum _Type {
    BY_INDEX = 0,
    BY_VALUE
} Type;

typedef struct _Num {
    int value;
    int compressed;
    int index;
} Num;


Num num[1000001];
Num sorted[1000001];

bool compare(const Num& left, const Num& right, Type type)
{
    if(type == BY_INDEX) {
        if(left.index <= right.index) return true;
        else return false;
    } else {
        if(left.value <= right.value) return true;
        else return false;
    }
}

void copy(Num& left, const Num& right)
{
    left.value = right.value;
    left.compressed = right.compressed;
    left.index = right.index;
}

void merge(Num* num, int start, int mid, int end, Type type)
{
    int i = start;
    int j = mid + 1;
    int k = start;

    while(i<=mid && j<=end) {
        if(compare(num[i], num[j], type)) copy(sorted[k++], num[i++]);
        else copy(sorted[k++], num[j++]);
    }

    if(i > mid) {
        for(unsigned int t = j; t<=end; t++) {
            copy(sorted[k++], num[t]);
        }
    } else {
        for(unsigned int t = i; t<=end; t++) {
            copy(sorted[k++], num[t]);
        }
    }

    for(unsigned int t = start ; t <= end ; t++) {
        copy(num[t], sorted[t]);
    }
}

void compress(Num* num, int size)
{
    for(unsigned int i = 0 ; i < size; i++) {
        if(i == 0) {
            num[0].compressed = 0;
            continue;
        }

        if(num[i-1].value == num[i].value) {
            num[i].compressed = num[i-1].compressed;
        } else {
            num[i].compressed = num[i-1].compressed + 1;
        }
    }
}

void merge_sort(Num* num, int start, int end, Type type) {
    if(start < end) {
        int mid = (start + end) / 2;
        merge_sort(num, start, mid, type);
        merge_sort(num, mid+1, end, type);
        merge(num, start, mid, end, type);
    }
}

int main(void)
{
    int N;
    scanf("%d", &N);
    
    for(int i = 0; i<N; i++) {
        int value;
        scanf("%d", &value);
        num[i].value = value;
        num[i].compressed = 0;
        num[i].index = i;
    }

    merge_sort(num, 0, N -1, BY_VALUE);
    compress(num, N);
    merge_sort(num, 0, N -1, BY_INDEX);
    
    for(unsigned int i = 0; i < N ; i++) {
        printf("%d ", num[i].compressed);
    }

    return 0;
}