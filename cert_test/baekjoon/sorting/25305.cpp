#include <iostream>

using namespace std;

void merge(unsigned int *table, int begin, int mid, int end)
{
    int i = begin;
    int j = mid + 1;
    int k = 0;
    int size = end - begin + 1;
    unsigned int *sorted = new unsigned int[size];

    while(i <= mid && j <= end) {
        if(table[i] >= table[j]) {
            sorted[k++] = table[i++];
        } else {
            sorted[k++] = table[j++];
        }
    }

    if(i > mid) {
        for(unsigned int t = j; t <=end ; t++) {
            sorted[k++] = table[t];
        }
    } else {
        for(unsigned int t = i; t <= mid; t++) {
            sorted[k++] = table[t];
        }
    }

    for(unsigned int t = 0; t < size; t++) {
        table[begin + t] = sorted[t];
    }

    delete [] sorted;
}

void merge_sort(unsigned int *table, int begin, int end)
{
    if(begin < end) {
        int mid = (begin + end) / 2;
        merge_sort(table, begin, mid);
        merge_sort(table, mid + 1, end);
        merge(table, begin, mid, end);
    }
}

int main(void)
{
    ios::sync_with_stdio(false);
    cin.tie(nullptr);

    int N, k;
    cin >> N >> k;

    unsigned int* table = new unsigned int[N];

    for(unsigned int i = 0; i < N; i++) {
        cin >> table[i];
    }
    
    merge_sort(table, 0, N - 1);
    cout << table[k-1] << '\n';
    
    return 0;
}