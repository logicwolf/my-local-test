#include <stdio.h>

typedef struct _Num {
    int value;
    int count;
    _Num() : value(0), count(0) {}
} Num;

int make_averge(int sum, int count)
{
    if(sum % count == 0) return sum / count;

    int res = 0;
    int mod = sum % count;
    int iaverage = sum / count;

    if(sum > 0) {
        if(count < 2*mod) res = iaverage + 1;
        else res = iaverage; 
    } else {
        if(-1*count > 2*mod) res = iaverage - 1;
        else res = iaverage;
    }

    return res;
}

int get_most_appeared(int* table, int size)
{
    int count_table[8001] = {0};
    int max = 0;
    int idx;
    for(unsigned int i = 0 ; i < size; i++) {
        idx = table[i]+4000;
        count_table[idx]++;
        if(max < count_table[idx]) max = count_table[idx];
    }

    int count = 0;
    for(unsigned int i = 0; i < 8001; i++) {
        if(count_table[i]==max) {
            if(count == 0) {
                count++;
                idx = i;
            } else if(count == 1) {
                idx = i;
                break;
            }
        }
    }

    return idx - 4000;
}

void merge(int* table, int start, int mid, int end)
{
    int i = start;
    int j = mid + 1;
    int k = 0;
    int size = end - start + 1;
    int* sorted = new int[size];

    while(i <= mid && j <= end) {
        if(table[i] <= table[j]) {
            sorted[k++] = table[i++];
        } else {
            sorted[k++] = table[j++];
        }
    }

    if(i > mid) {
        for(unsigned int t = j; t <= end; t++) {
            sorted[k++] = table[t];
        }
    } else {
        for(unsigned int t = i; t <= mid; t++) {
            sorted[k++] = table[t];
        }
    }

    for(unsigned int t = 0; t < size; t++) {
        table[start+t] = sorted[t];
    }

    delete[] sorted;
}

void merge_sort(int* table, int start, int end)
{
    if(start < end) {
        int mid = (start + end) / 2;
        merge_sort(table, start, mid);
        merge_sort(table, mid+1, end);
        merge(table, start, mid, end);
    }
}

int main(void)
{
    // int mod;
    // mod = -10 % 3;
    // int avg = -10 / 3; 
    // printf("avg : %d, mod : %d\n", avg, mod);

    // return 0;

    int N;
    scanf("%d", &N);

    int* num_table = new int[N]; 

    int sum = 0;
    int count[8001];
    for(unsigned int i = 0; i < N; i++) {
        scanf("%d", &num_table[i]);
        sum += num_table[i];
    }
    
    int average = make_averge(sum, N);
    merge_sort(num_table, 0, N - 1);

    printf("%d\n", average);
    printf("%d\n", num_table[N/2]);
    printf("%d\n", get_most_appeared(num_table, N));
    printf("%d\n", num_table[N-1] - num_table[0]);

    return 0;
}