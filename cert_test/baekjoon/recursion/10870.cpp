#include <stdio.h>

int fibonachi(int N)
{
    if(N == 0) return 0;
    if(N == 1) return 1;

    return fibonachi(N - 1) + fibonachi(N - 2);
}

int main(void)
{
    int N;
    scanf("%d", &N);

    printf("%d\n", fibonachi(N));

    return 0;
}