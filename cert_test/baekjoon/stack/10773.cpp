#include <iostream>

int stack[100001];
int pos = -1;


bool empty()
{
    if(pos == -1) return true;

    return false;
}

void push(int num)
{
    stack[++pos] = num;
}

int pop()
{
    if(empty() == true) return -1;

    int val = stack[pos];
    pos--;
    return val;
}

int main(void)
{
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int K;
    std::cin >> K;

    for(int i = 0; i < K; i++) {
        int num;
        std::cin >> num;

        if(num == 0) {
            pop();
        } else {
            push(num);
        }
    }

    int sum = 0;
    while(1) {
        if(empty()) break;
        sum += pop();
    }

    std::cout << sum << "\n";
    
    return 0;
}