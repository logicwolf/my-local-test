#include <iostream>

template <typename T>
class Stack {
public:
    Stack() = default;
    ~Stack() = default;

    void push(T val) {
        stack[++pos] = val;
    }

    T pop() {
        T val = stack[pos];
        pos--;
        return val;
    }

    T top() {
        return stack[pos];
    }

    bool empty() {
        if(pos == -1) return true;
        return false;
    }

private:
    T stack[101];
    int pos = -1;
};


bool balanced(const std::string& data)
{
    Stack<char> st_bal;

    for(int i = 0; i < data.size(); i++) {
        char ch = data[i];
        if(ch == '(' || ch == '[') {
            st_bal.push(ch);
        } else if(ch == ')') {
            if(st_bal.empty()) return false;
            if(st_bal.top() != '(') return false;
            st_bal.pop();
        } else if(ch == ']') {
            if(st_bal.empty()) return false;
            if(st_bal.top() != '[') return false;
            st_bal.pop();
        }
    }

    return st_bal.empty();
}

int main(void)
{
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    while(1) {
        char sentence[101] = {0};
        std::cin.getline(sentence, 101);
        // std::cout << sentence << "\n";
        std::string line = std::string(sentence);
        if(line.size() == 1 && line[0] == '.') break;

        if(balanced(line)) {
            std::cout << "yes\n";
        } else {
            std::cout << "no\n";
        }
    }

    return 0;
}