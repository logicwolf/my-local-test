#include <iostream>

using namespace std;

#define MAX_STACK_SIZE 100001

template <typename T>
class Stack {
public:
    Stack() = default;
    ~Stack() = default;

    T Pop() {
        return items[pos--];
    }
    T Top() {
        return items[pos];
    }
    void Push(T item) {
        items[++pos] = item;
    }
    
    bool Empty() {
        if(pos == -1) return true;
        return false;
    }
    unsigned int Size() {
        return (unsigned int) pos + 1;
    }

private:
    T items[MAX_STACK_SIZE];
    int pos = -1;
};

int main(void)
{
    ios::sync_with_stdio(false);
    cin.tie(nullptr);

    int N;
    cin >> N;

    char marker[MAX_STACK_SIZE];
    unsigned int index = 0;
    unsigned int num = 1;

    for(unsigned int i = 0; i < N; i++) {
        int cur_num;
        cin >> cur_num;

        
    }

    return 0;
}