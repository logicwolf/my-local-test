#include <iostream>

template <typename T>

class Stack {
public:
    bool empty() {
        if(pos == -1) return true;
        return false;
    }
    void push(T value) {
        stack[++pos] = value;
    }

    T pop() {
        T val = stack[pos];
        pos--;
        return val;
    }

    T top(){
        return stack[pos];
    }

private:
    int pos = -1;
    T stack[51];
};

bool check_vps(const std::string& data) {
    Stack<char> stc;

    for(int i = 0; i < data.size(); i++) {
        char ch  = data[i];
        if(ch == '(') {
            stc.push(ch);
        } else {
            if(stc.empty()) {
                return false;
            } else {
                stc.pop();
            }
        }
    }

    return stc.empty();
}

int main(void)
{
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int T;
    std::cin >> T;

    for(int i = 0; i < T; i++) {
        std::string input;
        std::cin >> input;

        if(check_vps(input)) {
            std::cout << "YES\n";
        } else {
            std::cout << "NO\n";
        }
    }

    return 0;
}