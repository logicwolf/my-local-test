#include <stdio.h>

typedef struct _Person {
    int x;
    int y;
    int rank;
} Person;

int main(void) 
{
    int N;
    scanf("%d", &N);

    Person person[50];

    for(unsigned int i = 0; i < N ; i++) {
        scanf("%d %d", &person[i].x, &person[i].y);
        person[i].rank = 1;
    }
    
    for(unsigned int i = 0; i < N ; i++) {
        for(unsigned int j = 0; j < N; j++) {
            if(i == j) continue;
            if(person[i].x < person[j].x && person[i].y < person[j].y) {
                person[i].rank = person[i].rank + 1;
            }
        }
    }

    for(unsigned int i = 0 ; i < N ; i++ ) {
        printf("%d\n", person[i].rank);       
    }

    return 0;
}