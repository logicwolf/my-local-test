#include <stdio.h>

int get_count(char init_ch, char* line, int line_size)
{
    int count = 0;
    for(unsigned int i = 0; i < line_size; i++) {
        if(i % 2 == 0 ) {
            if(line[i] != init_ch) count++;
        } else {
            if(line[i] == init_ch) count++;
        }
    }

    // printf("count: %d\n", count);

    return count;
}

int main(void)
{
    int N, M;
    scanf("%d %d", &N, &M);
    
    int count_B_start = 0;
    int count_W_start = 0;

    for(unsigned int i = 0; i < N; i++) {
        char* line = new char[M];
        scanf("%s", line);
        if(i % 2 == 0) count_B_start = count_B_start + get_count('B', line, M);
        else  count_B_start = count_B_start + get_count('W', line, M);

        if(i % 2 == 0) count_W_start = count_W_start + get_count('W', line, M);
        else  count_W_start = count_W_start + get_count('B', line, M);
    }

    if(count_B_start > count_W_start) 
        printf("%d\n", count_W_start);
    else 
        printf("%d\n", count_B_start);
    return 0;
}