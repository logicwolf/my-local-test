#include <stdio.h>

unsigned int start_point(unsigned int N)
{
    if(N <= 30) return 1;
    int temp = N;
    int count = 1;
    while(1) {
        temp = temp / 10;
        if(temp == 0) break;
        count++;
    }
    int ret  = N - 9*count;
    // printf("ret : %d, N:%u, count:%d\n", ret, N, count);
    return  (ret <=0) ? 1 : ret;
}

bool is_solution(int cand, int N)
{
    int sum = cand;
    // printf("cand:%d N:%d\n", cand, N);
    while(1) 
    {
        sum = sum + (cand % 10);
        cand = cand / 10;
        if(cand == 0) break;       
    }
    // printf("sum: %d\n", sum);
    if(sum == N) return true;
    else return false;
}

int main(void)
{
    unsigned int N;
    scanf("%u", &N);
    
    int res = 0;
    for(unsigned int i = start_point(N) ; i <= N ; i++) {
        if(is_solution(i, N) == true) {
            res = i;
            break;
        }
    }
    printf("%d\n", res);

    return 0;
}