#include <stdio.h>
// #include <algorithm>

void merge(unsigned int *num, int start, int mid, int end)
{
    int i = start;
    int j = mid + 1;
    int k = 0;
    int size = end - start + 1;
    unsigned int* sorted = new unsigned int[size];

    while(i <= mid && j <= end) {
        if(num[i] <= num[j]) sorted[k++] = num[i++];
        else sorted[k++] = num[j++];
    }

    if( i > mid) {
        for(int t = j; t <=end; t++) {
            sorted[k++] = num[t];
        }
    } else {
        for(int t = i; t <=end; t++) {
            sorted[k++] = num[t];
        }
    }

    for(int t = 0; t < size; t++) {
        num[start + t] = sorted[t];
    }

    delete[] sorted;
}

void merge_sort(unsigned int *num, int start, int end)
{
    if(start < end) {
        unsigned long mid = (start + end) / 2;
        merge_sort(num, start, mid);
        merge_sort(num, mid+1, end);
        merge(num, start, mid, end);
    }
}

int main(void)
{
    int n;

    scanf("%d", &n);
    unsigned int *nums = new unsigned int[n];

    for(int i = 0; i < n; i++) {
        scanf("%u", &nums[i]);
    }

    merge_sort(nums, 0, n - 1);
    // std::sort(nums, nums + n);

    printf("%u\n", nums[0]*nums[n-1]);

    delete[] nums;

    return 0;
}