#include <stdio.h>

int main(void)
{
    int N, K;
    scanf("%d %d", &N, &K);

    if(K > N - K) {
        K = N - K;
    }

    int A = 1;
    int B = 1;
    for(int i = 1; i <= K; i++) {
        A = A * (N - i + 1);
        B = B * i; 
    }

    printf("%d\n", A / B);

    return 0;
}