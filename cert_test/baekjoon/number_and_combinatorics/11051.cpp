#include <stdio.h>

int main(void)
{
    int N, K;
    scanf("%d %d", &N, &K);

    if(K > N - K) {
        K = N - K;
    }

    unsigned int A = 1;
    unsigned int B = 1;
    for(int i = 1; i <= K; i++) {
        A = (A * (N - i + 1)) % 10007;
        B = (B * i)  % 10007; 
    }

    printf("%u\n", A / B);

    return 0;
}