#include <stdio.h>

int res[50];

int factorial(int n)
{
    if (n == 0 || n == 1) return 1;

    if(res[n] != 0) return res[n];

    res[n] = n*factorial(n-1);

    return res[n];
}

int count_n_division(int number, int div)
{
    int count = 0;
    while (1) {
        if(number % div != 0) break;
        count++;
        number /= div;
    }

    return count;
}

int main(void)
{
    int n = 0;
    scanf("%d", &n);

    int cnt_2 =0;
    int cnt_5 =0;
    for(int i = 1; i <= n; i++) {
        // cnt_2 += count_n_division(i, 2);
        cnt_5 += count_n_division(i, 5);
    }

    // printf("%d %d\n", cnt_2, cnt_5);

    int count = 0;
    printf("%d\n", cnt_5);

    return 0;
}