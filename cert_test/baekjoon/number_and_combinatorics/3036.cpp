#include <stdio.h>

void divide(int &A, int &B)
{
    int num = 2;
    while(num <= A && num <= B) {
        if(A % num == 0 && B % num == 0) {
            A = A / num;
            B = B / num;
        } else {
            num++;
        }
    }
    // printf("A:%d, B:%d", A, B);
}

int main(void)
{
    int n;
    scanf("%d", &n);
    int *nums = new int[n];

    for(int i = 0 ; i < n; i++) {
        scanf("%d", &nums[i]);
    }

    for(int i = 1; i < n; i++) {
        int A = nums[0];
        int B = nums[i];
        divide(A, B);
        printf("%d/%d\n", A, B);
    }

    delete[] nums;

    return 0;
}