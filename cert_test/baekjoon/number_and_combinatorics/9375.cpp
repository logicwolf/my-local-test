#include <stdio.h>
#include <map>
#include <string>

int main(void)
{
    int N;
    scanf("%d", &N);

    for(int tc = 0; tc < N; tc++) {
        int n;
        scanf("%d", &n);
        std::map<std::string, int> items;

        for(int i=0; i< n; i++) {
            char name[21] = {0,};
            char type[21] = {0,};

            scanf("%s %s", name, type);
            auto it = items.find(std::string(type));
            if(it == items.end()) {
                items[std::string(type)] = 1; 
            } else {
                it->second = it->second+1;
            }  
        }

        int res = 1;
        for(auto it : items) {
            res = res * (it.second + 1);
        }
        res -= 1;
        printf("%d\n", res);
    }

    return 0;
}