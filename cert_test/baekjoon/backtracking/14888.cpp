#include <stdio.h>

#define PLUS 0
#define MINUS 1
#define MULTI 2
#define DIVIDE 3

int n;
int numbers[12];

int operators[11];
int sel_ops[11];
bool visited[11] = {false,};

int max = -1000000000;
int min = 1000000000;

int value = 0;

int calculate()
{
    int result=0;
    result = numbers[0];
    for(int i = 0; i< n-1; i++) {
        int op = sel_ops[i];
        if( op == PLUS) {
            result = result + numbers[i+1];
        } else if(op == MINUS) {
            result = result - numbers[i+1];
        } else if(op == MULTI) {
            result = result * numbers[i+1];
        } else {
            result = result / numbers[i+1]; // this should be check for negative value 
        }
    }

    return result;
}

void minmax(int idx)
{
    if(idx == n - 1)
    {
        int result = calculate();
        if(result > max) max = result;
        if(result < min) min = result;

        return;
    }

    for(int i = 0 ; i < n - 1 ; i++) {
        if(visited[i] == true) continue;
        sel_ops[idx] = operators[i];
        visited[i] = true;
        minmax(idx + 1);
        visited[i] = false;
    }
}

int main(void)
{
    scanf("%d", &n);
    for(int i = 0; i < n ; i++) {
        scanf("%d", &numbers[i]);
    }

    int op_count = 0;
    for(int i = PLUS; i <= DIVIDE; i++) {
        int op;
        scanf("%d", &op);

        for(int k = 0; k < op;k++) {
            operators[op_count++] = i;
        }
    }

    minmax(0);
    printf("%d\n%d\n", max, min);

    return 0;
}