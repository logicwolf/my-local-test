#include <stdio.h>

#define MAX 8
int table[MAX];
int visit[MAX];

int n, m;

void find_solution(int count)
{
    if(count == m)
    {
        for(int i = 0; i < m; i++) {
            printf("%d ", table[i]);
        }
        printf("\n");
        return;
    }

    for(int i = 1;i<=n; i++) {
        table[count] = i;
        find_solution(count + 1);
    }
}

int main(void)
{
    scanf("%d %d", &n, &m);

    find_solution(0);

    return 0;
}