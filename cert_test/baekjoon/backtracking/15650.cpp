#include <stdio.h>

#define MAX 9
int table[MAX] = {0,};
bool visit[MAX] = {false,};
int n, m;

void make_list(int num, int count)
{
    if(count == m) {
        for(int i = 0 ; i < m ; i++) {
            printf("%d ", table[i]);
        }
        printf("\n");
        return;
    }

    for(int i = num; i <= n; i++) {
        if(visit[i] == false) {
            visit[i] = true;
            table[count] = i;
            make_list(i+1, count + 1);
            visit[i] = false;
        }
    }
}

int main(void)
{
    scanf("%d %d", &n, &m);
    make_list(1, 0);
    return 0;
}