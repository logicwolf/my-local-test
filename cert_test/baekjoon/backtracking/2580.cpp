#include <stdio.h>

typedef struct _Point {
    int x;
    int y;
} Point;

Point point[81];
int empty_count = 0;

int table[9][9];
bool is_solved = false;

bool is_possible(int x, int y, int num)
{
    // printf("x:%d, y:%d, num:%d\n", x, y, num);
    for(int i = 0; i < 9; i++) {
        if(table[y][i] == num) {
            // printf("y:%d, i:%d, num:%d is false\n", y, i, num);
            return false;
        }
        if(table[i][x] == num) {
            // printf("i:%d, x:%d, num:%d is false\n", i, x, num);
            return false;
        }
    }

    int start_x = (x / 3) * 3;
    int start_y = (y / 3) * 3;
    // printf("start x:%d, start y:%d\n", start_x, start_y);

    for(int i = start_y; i < start_y + 3; i++) {
        for(int j = start_x; j < start_x + 3; j++) {
            if(table[i][j] == num) {
                // printf("i:%d, j:%d, num:%d is false\n", i, j, num);
                return false;
            }
        }
    }
    // printf("x:%d, y:%d, num:%d is candidate\n", x, y, num);
    return true;
}

void print_table()
{
    for(int i = 0; i < 9; i++) {
        for( int j =0 ; j < 9 ; j++) {
            printf("%d ", table[i][j]);
        }
        printf("\n");
    }
}

void do_complete(int count) 
{
    // printf("count : %d\n", count);
    if(count == empty_count) {
        print_table();
        is_solved = true;
        return;
    }

    for(int i = 1; i <=9; i++) {
        
        if(is_possible(point[count].x, point[count].y, i) == false) continue;
        table[point[count].y][point[count].x] = i;
        do_complete(count+1);
    
        if(is_solved) return;
    }    
    table[point[count].y][point[count].x] = 0;
}

int main(void)
{
    for(int i = 0; i < 9; i++) {
        for(int j = 0; j < 9 ; j++) {
            scanf("%d", &table[i][j]);
            if(table[i][j] == 0) {
                point[empty_count].x = j;
                point[empty_count].y = i;
                empty_count++;
            }
        }
    }

    do_complete(0);

    return 0;
}