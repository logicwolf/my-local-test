#include <stdio.h>

int main(void)
{
    while(1) {
        unsigned int x, y, z;
        scanf("%u %u %u", &x, &y, &z);

        if(x == 0 && y == 0 && z == 0) {
            break;
        }

        unsigned int x_sqr = x*x;
        unsigned int y_sqr = y*y;
        unsigned int z_sqr = z*z;

        if((x_sqr == y_sqr + z_sqr)
        || (y_sqr == x_sqr + z_sqr)
        || (z_sqr == x_sqr + y_sqr)) {
            printf("right\n");
        } else {
            printf("wrong\n");
        }
    }

    return 0;
}