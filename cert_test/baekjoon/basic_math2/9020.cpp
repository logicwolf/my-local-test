#include <stdio.h>

int main(void)
{
    unsigned int T;
    scanf("%u", &T);

    unsigned int *n = new unsigned int[T];

    // 입력 받기
    int largest_num = 0; 
    for(unsigned int i = 0 ; i < T; i++) {
        scanf("%u", &n[i]);
        if(n[i] > largest_num) largest_num = n[i];
    }

    // 에라토스테네스의 체 만들기
    bool *table = new bool[largest_num+1];
    for(unsigned int i = 0 ; i <= largest_num;i++) {
        table[i] = true;
    }

    for(unsigned int i = 2; i <= largest_num; i++)
    {
        if(table[i] == false) continue;
        for(unsigned int j = i+i; j < largest_num; j += i) {
            table[j] = false;
        }
    }

    for(unsigned int i = 0 ; i < T; i++) {
        for(unsigned int j = n[i] / 2; j >= 0 ; j--) {
            if(table[j] == true && table[n[i] - j]) {
                printf("%d %d\n", j, n[i] - j);
                break;
            }
        }
    }

    delete []n;
    delete []table;

    return 0;
}