#include <stdio.h>

int main(void)
{
    unsigned int R;
    scanf("%u", &R);

    // printf("%u\n", R);

    long double pi = 3.141592653589793238462643383279502;
    long double R_sqr = R * R;
    // printf("%lf\n", R_sqr);
    long double taxi_size = R_sqr * 2;
    long double euclid_size = R_sqr * pi;

    printf("%llf\n", euclid_size);
    printf("%llf\n", taxi_size);

    return 0;
}