#include <stdio.h>

int score[301];
int max[301];

int get_max(int a, int b)
{
    if(a < b) return b;
    else return a;
}

int main(void)
{
    int n;
    scanf("%d", &n);

    for(int i = 1; i <= n; i++) {
        scanf("%d", &score[i]);
    }

    max[1] = score[1];
    max[2] = max[1] + score[2];
    max[3] = get_max(score[1]+score[3], score[2]+score[3]);

    for(int i = 4;i <=n ; i++) {
        max[i] = get_max(max[i-2]+score[i], max[i -3]+score[i-1]+score[i]);
    }

    printf("%d\n", max[n]);
    
    return 0;
}