#include <stdio.h>

int max(int a, int b)
{
    if(a > b) return a;
    return b;
}

int main(void)
{
    int n;
    scanf("%d", &n);
    int value[10001];
    int res[10001];

    for(int i = 1;i<=n;i++) {
        scanf("%d", &value[i]);
    }

    res[1] = value[1];
    res[2] = res[1] + value[2];
    res[3] = max(res[1] + value[3], value[2] + value[3]);
    res[3] = max(res[3], res[2]);

    for(int i = 4; i<=n;i++) {
        res[i] = max(res[i-2] + value[i], res[i-3] + value[i-1] + value[i]);
        res[i] = max(res[i], res[i-1]);
    }

    printf("%d\n", res[n]);
    return 0;
}