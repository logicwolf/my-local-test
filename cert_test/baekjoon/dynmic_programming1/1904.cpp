#include <stdio.h>

int main(void)
{
    int n;
    scanf("%d", &n);

    int *count = new int[n+1];
    count[0] = 0;
    count[1] = 1;
    count[2] = 2;

    for(int i = 3; i <= n; i++) {
        count[i] = count[i-1] + count[i-2];
        count[i] = count[i] %  15746;
    }

    printf("%d\n", count[n]);

    delete[] count;

    return 0;
}

// 1 : 1
// 2 : 00, 11
// 3 : 001, 100, 111
// 4 : 0011, 1001, 1111, 0000, 1100
// 5 : 00111, 10011, 11111, 00001, 11001, 00100, 10000, 11100
// 6 : 000000, 000011, 001001, 001100, 001111, 010011, 011001, 011100, 011111, 100001, 100111, 110011, 111001, 111100, 111111





