#include <stdio.h>

int max(int a, int b) {
    if(a > b) return a;
    return b;
}

int main(void)
{
    int n;
    scanf("%d", &n);

    int array[1001];
    for(int i =1; i <=n ; i++) {
        scanf("%d", &array[i]);
    }

    int DP[1001];
    int REV_DP[1001];
    DP[1] = 1;
    REV_DP[n] = 1;

    for(int i = 2; i <= n ; i++) {
        DP[i] = 1;
        for(int j = i - 1; j >= 1; j--) {
            if(array[i] > array[j]) DP[i] = max(DP[i], DP[j] + 1);
        }
    }

    for(int i = n; i >= 1; i--) {
        REV_DP[i] = 1;
        for(int j = i + 1; j <= n; j++) {
            if(array[i] > array[j]) REV_DP[i] = max(REV_DP[i], REV_DP[j] + 1);
        }
    }

    int max = 1;
    for(int i = 1; i <= n; i++) {
        int sum = DP[i] + REV_DP[i];
        if(sum > max) max = sum;
    }

    printf("%d\n", max - 1);
    
    return 0;
}