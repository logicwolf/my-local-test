#include <stdio.h>

int main(void)
{
    int n;
    scanf("%d", &n);

    int array[1001];
    for(int i = 1; i <= n; i++) {
        scanf("%d", &array[i]);
    }

    int DP[1001];
    DP[1] = 1;
    int max = 1;
    for(int i = 2; i<= n; i++) {
        DP[i] = 1;
        for(int j = i - 1 ; j >= 1; j--) {
            if(array[i] > array[j]) {
                if(DP[i] < DP[j] + 1) DP[i] = DP[j] + 1;
            }
        }
        if(max < DP[i]) max = DP[i];
    }

    printf("%d\n", max);

    return 0;
}