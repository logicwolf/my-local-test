#include <stdio.h>

#define MOD 1000000000

int main(void)
{
    int n;
    scanf("%d", &n);

    int res_table[102][10];
    res_table[1][0] = 0;
    for(int i = 1; i <=9 ; i++) {
        res_table[1][i] = 1;
    }

    for(int i = 2; i <= n; i++) {
        for(int j = 0; j <= 9; j++) {
            if(j == 0) {
                res_table[i][0] = res_table[i - 1][1];
            } else if(j == 9) {
                res_table[i][9] = res_table[i - 1][8];
            } else {
                res_table[i][j] = res_table[i - 1][j - 1] + res_table[i - 1][j + 1];
            }

            res_table[i][j] %= MOD;
        }
    }    

    int result = 0;
    for(int i = 0; i <= 9; i++) {
        result = (result + res_table[n][i]) % MOD;
    }
    printf("%d\n", result);

    return 0;
}