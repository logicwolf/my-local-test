#include <stdio.h>

int Max(int a, int b)
{
    if(a > b) return a;
    return b;
}

int main(void)
{
    char str1[1001] = {0,};
    char str2[1001] = {0,};

    scanf("%s", str1 + 1);
    scanf("%s", str2 + 1);

    int DP[1002][1002] = {0,};
    int i;
    int j;

    for(i = 1 ; str1[i]; i++) {
        for(j = 1; str2[j]; j++) {
            if(str1[i] == str2[j]) {
                DP[i][j] = DP[i - 1][j - 1] + 1;
            } else {
                DP[i][j] = Max(DP[i - 1][j], DP[i][j - 1]);
            }
        }
    }

    printf("%d\n", DP[i-1][j-1]);

    return 0;    
}