#include <stdio.h>

int dp[101][100001];

int Max(int a, int b)
{
    if(a > b) return a;
    return b;
}

int main(void)
{
    int N, K;
    scanf("%d %d", &N, &K);

    int w[N+1] = {0,};
    int v[N+1] = {0,};

    for(int i = 1; i <= N; i++) {
        scanf("%d %d", &w[i], &v[i]);
    }

    
    for(int i = 1; i <= N; i++) {
        for(int j = 1; j <= K; j++) {
            if(j - w[i] >= 0) {
                dp[i][j] = Max(dp[i-1][j], dp[i-1][j-w[i]] + v[i]);
            } else {
                dp[i][j] = dp[i-1][j];
            }
        }
    }

    // for(int i = 1;i <= N; i++) {
    //     for(int j =1 ; j <= K; j++) {
    //         printf("%d ", dp[i][j]);
    //     }
    //     printf("\n");
    // }

    printf("%d\n", dp[N][K]);

    return 0;
}
