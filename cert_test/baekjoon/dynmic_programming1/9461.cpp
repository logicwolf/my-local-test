#include <stdio.h>

long long length[101];

long long get_length(int N)
{
    length[1] = 1;
    length[2] = 1;
    length[3] = 1;
    length[4] = 2;
    length[5] = 2;

    if(N <= 5) {
        return length[N];
    }

    for(int i = 6; i <=N; i++) {
        length[i] = length[i-1] + length[i-5];
    }

    return length[N];
}

int main(void)
{
    int tc;
    scanf("%d", &tc);

    for(int i = 0; i < tc;i++) {
        int N;
        scanf("%d", &N);
        printf("%lld\n", get_length(N));
    }

    return 0;
}