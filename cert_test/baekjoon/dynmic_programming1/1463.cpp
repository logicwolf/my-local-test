#include <stdio.h>

int min_table[1000001];
#define VERY_LARGE_NUM 1000000000;

int main(void)
{
    int n;
    scanf("%d", &n);

    min_table[1] = 0;
    min_table[2] = 1;

    for(int i = 3; i<=n;i++) {
        int min = VERY_LARGE_NUM;
        if(i % 3 == 0) {
            if(min > min_table[i/3]) min = min_table[i/3];
        }
        
        if(i % 2 == 0) {
            if(min > min_table[i/2]) min = min_table[i/2];
        }

        if(min > min_table[i - 1]) min = min_table[i - 1];

        min_table[i] = min + 1;
    }

    printf("%d\n", min_table[n]);

    return 0;
}