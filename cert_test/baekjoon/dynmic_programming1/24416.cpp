#include <stdio.h>

int fibo[41];

int main(void)
{
    int N;
    scanf("%d", &N);

    fibo[1] = fibo[2] = 1;
    for(int i = 3; i<= N; i++) {
        fibo[i] = fibo[i-1] + fibo[i-2];
    }

    printf("%d %d\n", fibo[N], N - 2);
    
    return 0;
}