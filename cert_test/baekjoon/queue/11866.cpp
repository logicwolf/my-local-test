#include <iostream>

using namespace std;

#define Q_MAX_SIZE 600000

class Queue {
public:
    void Push(int item) {
        items[rear++] = item;
    }

    int Pop() {
        return items[front++];
    }

    int Front() {
        return items[front];
    }

    int Back() {
        return items[rear -1];
    }

    bool Empty() {
        if(front == rear) return true;
        return false;
    }

    unsigned int Size() {
        return rear - front;
    }

private:
    int items[Q_MAX_SIZE];
    int front = 0;
    int rear =0;
};


int main(void)
{
    ios::sync_with_stdio(false);
    cin.tie(nullptr);

    int N, K;
    cin >> N >> K;

    cout << "<";

    Queue q;
    for(int i = 1; i <= N; i++) {
        q.Push(i);
    }

    while(q.Size() != 1) {
        for(unsigned int i = 1; i < K;i++) {
            q.Push(q.Pop());
        }
        cout << q.Pop() << ", ";
    }

    cout << q.Pop() << ">" << '\n';

    return 0;
}