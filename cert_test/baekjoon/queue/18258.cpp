#include <iostream>
#include <string>

using namespace std;

#define Q_MAX_SIZE 2000000

class Queue {
public:
    void Push(int item) {
        items_[rear_++] = item;
    }

    int Pop() {
        if(Empty() == 1) return -1;
        return items_[front_++];
    }

    unsigned int Size() {
        return rear_ - front_;
    }

    int Empty() {
        if(front_ == rear_) return 1;
        return 0;
    }

    int Front() {
        if(Empty() == 1) return -1;
        return items_[front_];
    }

    int Back() {
        if(Empty() == 1) return -1;
        return items_[rear_-1];
    }

private:
    int items_[Q_MAX_SIZE];
    unsigned int front_ = 0;
    unsigned int rear_ = 0;
};

int main(void)
{
    ios::sync_with_stdio(false);
    cin.tie(nullptr);

    int N;
    cin >> N;
    Queue num_q;
    for(unsigned int i = 0 ; i < N; i++) {
        string command;
        cin >> command;
        if(command == "push") {
            int num;
            cin >> num;
            num_q.Push(num);
        } else if(command == "pop") {
            cout << num_q.Pop() << '\n';
        } else if(command == "size") {
            cout << num_q.Size() << '\n';
        } else if(command == "empty") {
            cout << num_q.Empty() << '\n';
        } else if(command == "front") {
            cout << num_q.Front() << '\n';
        } else if(command =="back") {
            cout << num_q.Back() << '\n';
        }
    }

    return 0;
}

