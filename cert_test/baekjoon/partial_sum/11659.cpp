#include <stdio.h>

int table[100001];
int sum_table[100001];
int main(void)
{
    int N, M;
    scanf("%d %d", &N, &M);

    sum_table[0] = 0;
    for(int i = 1; i <= N; i++) {
        scanf("%d", &table[i]);
        sum_table[i] = sum_table[i - 1] + table[i];
    }
    for(int i = 1; i <= M; i++) {
        int start, end;
        scanf("%d %d", &start, &end);
        printf("%d\n", sum_table[end] - sum_table[start - 1]);
    }

    return 0;
}