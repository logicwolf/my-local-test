#include <iostream>


int count[1001];

int main(void)
{
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);


    int N, M;
    std::cin >> N >> M;
    
    int sum = 0;
    for(int i = 0; i < N; i++) {
        int num;
        std::cin >> num;
        sum += num;
        count[sum = (sum % M)]++;
    }

    int result = count[0];
    for(int i = 0; i < M; i++) {
        result += (count[i] * (count[i] - 1) / 2);
    }

    std::cout << result << '\n';

    return 0;
}