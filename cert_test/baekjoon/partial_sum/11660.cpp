#include <stdio.h>

int table[1025][1025];
int sum_table[1025][1025];

void swap(int& a, int&b)
{
    int temp;
    temp = a;
    a = b;
    b = temp;
}

void print_table(int map[][1025], int size)
{
    for(int i = 1; i <= size; i++) {
        for(int j = 1; j <= size; j++) {
            printf("%d ", map[i][j]);
        }
        printf("\n");
    }
}

int main(void)
{
    int N, M;

    scanf("%d %d", &N, &M);

    for(int i = 1; i <= N; i++) {
        for(int j = 1; j <= N; j++) {
            scanf("%d", &table[i][j]);
            if(i == 1 && j == 1) {
                sum_table[1][1] = table[1][1];
            } else if(i == 1) {
                sum_table[i][j] = sum_table[i][j - 1] + table[i][j];
            } else if(j == 1) {
                sum_table[i][j] = sum_table[i - 1][j] + table[i][j];
            } else {
                sum_table[i][j] = sum_table[i - 1][j] + sum_table[i][j - 1] + table[i][j] - sum_table[i-1][j-1];
            }
        }
    }

    for(int t = 1; t <= M; t++) {
        int x1, y1, x2, y2;
        scanf("%d %d %d %d", &x1, &y1, &x2, &y2);
        
        if(x1 > x2) swap(x1, x2);
        if(y1 > y2) swap(y1, y2);

        int result = sum_table[x2][y2];
        
        if(x1 == 1 && y1 == 1) {
            /* skip */
        } else if(x1 == 1) {
            result = result - sum_table[x2][y1 -1];
        } else if (y1 == 1) {
            result = result - sum_table[x1 - 1][y2];
        } else {
            result = result - sum_table[x2][y1 - 1] - sum_table[x1 -1][y2] + sum_table[x1 -1][y1 - 1]; 
        }

        printf("%d\n", result);
    }
    // printf("\n===================================\n");
    // print_table(table, N);
    // printf("\n===================================\n");
    // print_table(sum_table, N);

    return 0;
}