#include <stdio.h>
#include <string.h>

int atoi(char ch)
{
    return ch - '0';
}

int main(void)
{
    char A[10001] = {0,};
    char B[10001] = {0,};
    unsigned int sum[10002];

    for(unsigned int i = 0; i < 10002; i++) {
        sum[i] = 0;
    }
    scanf("%s %s", A, B);

    int idxA = strlen(A) - 1;
    int idxB = strlen(B) - 1;
    int idxSum =0;
    while(1) {
        int local_sum = 0;
        if(idxA >= 0 && idxB >= 0) {
            local_sum = sum[idxSum] + atoi(A[idxA--]) + atoi(B[idxB--]); 
        } else if(idxA >= 0 && idxB < 0) {
            local_sum = sum[idxSum] + atoi(A[idxA--]);
        } else if(idxA < 0 && idxB >= 0) {
            local_sum = sum[idxSum] + atoi(B[idxB--]);
        } else if(idxA < 0 && idxB < 0) break;

        // printf("local sum : %u\n", local_sum);
        if(local_sum >= 10) {
            sum[idxSum] = local_sum - 10;
            sum[idxSum+1] = 1;
        } else {
            sum[idxSum] = local_sum;
        }
        idxSum++;
    }
    // printf("idxSum : %u\n", idxSum);

    if(sum[idxSum] != 0) printf("%u", sum[idxSum]);

    for(int i = idxSum -1; i >= 0; i--) 
    {
        // printf("i = %d", i);
        printf("%u", sum[i]);
    }
    printf("\n");
    return 0;
}