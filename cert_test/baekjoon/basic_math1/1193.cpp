#include <stdio.h>

unsigned int get_an(int num)
{
    return num * (num + 1) / 2;
}

int main(void)
{
    unsigned int N;
    scanf("%d", &N);
    // printf("%d\n", get_an(N));
    int idx = 1;
    while (N > get_an(idx)) {

        idx++;
    }

    if(idx == 1) {
        printf("1/1\n");
        return 0;
    }

    int diff = N - get_an(idx - 1);

    if( idx % 2 == 0) {

        printf("%d/%d\n", diff, idx-diff + 1);
    } else {
        printf("%d/%d\n", idx-diff+1, diff);
    }


    return  0;
}