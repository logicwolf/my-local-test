#include <iostream>
#include <map>
#include <string>
#include <sstream>

int main(void)
{
    int N, M;
    scanf("%d %d", &N, &M);

    std::map<int,std::string> int_map;
    std::map<std::string,int> str_map;
    for(int i = 1; i <= N; i++) {
        char name[21] = {0,};
        scanf("%s", name);
        int_map[i] = name;
        str_map[name] = i;
    }

    for(int i = 1; i <=M; i++) {
        char input[21] = {0,};
        scanf("%s", input);
        if(input[0] >= '0' && input[0] <='9') {
            int num = 0;
            std::stringstream ss(input);
            ss >> num;

            printf("%s\n", int_map[num].c_str());
        } else {
            printf("%d\n", str_map[input]);
        }
    }

    return 0;
}