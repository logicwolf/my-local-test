#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

int main(void)
{
    int N, M;
    std::cin >> N >> M;

    std::vector<std::string> table;
    for(int i = 0 ; i < M+N; i++) {
        std::string name;
        std::cin >> name;

        table.push_back(name);
    }

    sort(table.begin(), table.end());
    
    std::vector<std::string> result;
    int count = 0;
    for(int i=1; i < table.size(); i++) {
        if(table[i] == table[i-1]) {
            result.push_back(table[i]);
            count++;
        }
    }
    std::cout << count << std::endl;
    for(int i = 0; i < result.size(); i++) {
        std::cout << result[i] << std::endl;
    }

    return 0;
}