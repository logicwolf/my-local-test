#include <stdio.h>
#include <map>

int main(void)
{
    int a, b;
    scanf("%d %d", &a, &b);
    std::map<int, int> m;
    for(int i = 0; i < a; i++) {
        int A;
        scanf("%d", &A);
        m[A]++;
    }
    int count_common = 0;
    for(int i = 0; i < b; i++) {
        int B;
        scanf("%d", &B);
        if(m[B]) count_common++;
    }

    printf("%d\n", a+b - 2*count_common);

    return 0;
}