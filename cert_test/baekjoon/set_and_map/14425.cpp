#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

bool search(const std::vector<std::string>& set, const std::string& word)
{
    int begin = 0; 
    int end = set.size() - 1;
    
    while(begin <= end) {
        int middle = (begin + end) / 2;
        if(set[middle].compare(word) == 0) {
            return true;
        } else if(set[middle].compare(word) < 0) {
            begin = middle + 1;
        } else {
            end = middle - 1;
        }
    }
    return false;
}

int main(void)
{
    int N, M;
    std::cin >> N >> M;

    std::vector<std::string> set;
    for(int i = 0; i < N; i++) {
        std::string word;
        std::cin >> word;
        set.push_back(word);
    }

    sort(set.begin(), set.end());

    int count=0;
    for(int i = 0; i < M; i++) {
        std::string test;
        std::cin >> test;

        if(search(set, test)) {
            count++;
        }
    }

    printf("%d\n", count);

    return 0;
}