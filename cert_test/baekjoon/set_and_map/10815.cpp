#include <stdio.h>

void print_array(int array[], int size)
{
    for(int i = 0; i < size; i++) {
        printf("%d ", array[i]);
    }
    printf("\n");
}

void merge(int* table, int begin, int middle, int end)
{
    int i = begin;
    int j = middle + 1;
    int k = 0;
    int size = end - begin + 1;
    int *sorted = new int[size];

    while(i <= middle && j <= end) {
        if(table[i] < table[j]) sorted[k++] = table[i++];
        else sorted[k++] = table[j++];
    }

    if(i > middle) {
        for(int t = j; t <= end; t++) {
            sorted[k++] = table[t];
        }
    } else {
        for(int t = i; t <= middle; t++) {
            sorted[k++] = table[t];
        }
    }

    for(int t = 0; t < size; t++) {
        table[begin+t] = sorted[t];
    }

    delete[] sorted;
}

void merge_sort(int* table, int begin, int end)
{
    if(begin < end) {
        int middle = (begin + end) / 2;
        merge_sort(table, begin, middle);
        merge_sort(table, middle + 1, end);
        merge(table, begin, middle, end);
    }
}

bool search(int* array, int size, int num)
{
    int begin = 0;
    int end = size - 1;

    while(begin <= end) {
        int middle = (begin + end) /2;
        if(array[middle] == num) {
            return true;
        } else if(array[middle] < num) {
            begin = middle + 1;
        } else {
            end = middle - 1;
        }
    }

    return false;
}

int main(void)
{
    int N;
    scanf("%d", &N);
    int set[N];
    for(int i = 0; i < N; i++) {
        scanf("%d", &set[i]);
    }

    merge_sort(set, 0, N -1);
    
    int M;
    scanf("%d", &M);
    for(int i = 0; i < M; i++) {
        int num;
        scanf("%d", &num);
        if(search(set, N, num)) {
            printf("1 ");
        } else {
            printf("0 ");
        }
    }
    printf("\n"); 

    return 0;
}