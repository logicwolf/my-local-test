#include <stdio.h>
#include <string.h>

int get_time(int num)
{
    if(num == 0) return 11;

    return num+1;
}

int get_num(char ch)
{
    if(ch >= 'A' && ch <= 'C') return 2;
    else if(ch >= 'D' && ch <= 'F') return 3;
    else if(ch >= 'G' && ch <= 'I') return 4;
    else if(ch >= 'J' && ch <= 'L') return 5;
    else if(ch >= 'M' && ch <= 'O') return 6;
    else if(ch >= 'P' && ch <= 'S') return 7;
    else if(ch >= 'T' && ch <= 'V') return 8;
    else if(ch >= 'W' && ch <= 'Z') return 9;
    else return 0;
}

int main(void)
{
    char input[20] = {0,};
    scanf("%s", input);

    int time = 0;

    for(unsigned int i = 0; i < strlen(input) ; i++) {
        int num = get_num(input[i]);
        time += get_time(num);
    }

    printf("%d\n", time);

    return 0;
}