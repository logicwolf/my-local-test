#include <stdio.h>
#include <string.h>

bool is_group_word(char word[])
{
    if(strlen(word) == 1) return true;
    
    bool table[26];
    for(unsigned int i = 0;i < 26; i++) {
        table[i] = false;
    }

    char cur = word[0];
    char next;
    table[cur - 'a'] = true;
    for(unsigned int i = 1; i < strlen(word) ; i++) {
        next = word[i];
        if(cur != next) {
            if(table[next - 'a'] == true) return false;
            table[next - 'a'] = true;
        }
        cur = next;
    }

    return true;
}

int main(void)
{
    int N;
    scanf("%d", &N);
    int count = 0;
    for(int tc = 0; tc < N; tc++) {
        char word[101] = {0,};
        scanf("%s", word);
        bool res = is_group_word(word);
        // printf("%s\n", res ? "true" : "false");
        if(res) count++;
    }
    printf("%d\n", count);

    return 0;
}